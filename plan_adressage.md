# Plan d'addressage ip

## Tableaux d'addressage ip

|MACHINE|IP|VLAN|
|---|---|---|
|SW1|10.0.0.254|100|
|SW2|20.0.0.254|200|
|SYS|150.0.0.1|300|

## Schéma

```mermaid
classDiagram
	Syslog <|-- SW1
	Syslog <|-- SW2

       Syslog: Config. Réseaux
       Syslog: --
       Syslog: IP : 150.0.0.1
       Syslog: Mask : 255.255.255.0
       Syslog: --
       Syslog: vlan(300) 

	class SW1{
               Config. Réseaux
               --
              IP : 10.0.0.254
              Mask : 255.255.255.0
              --
              vlan(100)
	}

	class SW2{
               Config. Réseaux
               --
              IP : 20.0.0.254
              Mask : 255.255.255.0
              --

              vlan(200)
	}
```
