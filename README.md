# Ansible config switch

|Groupe|
|---|
|Axel|
|Dorian|
|Florentin|
|Jeremy|


## Sujet

> Nous avons pensé à automatiser sur des équipements réseaux switch de marque Cisco, toute une configuration pour les sécuriser à maximum.
> C'est à dire mettre en place toute les sécurités recommander par l'ANSSI sur des switchs cisco pour diminuer le risque d'attaque sur ces équipements réseaux.
> Nous allons effectuer cela sur GNS3, c'est un émulateur d'environnement virtuel pour simuler des switchs.
> Puis créer une VM pour la centralisation des logs.

[Plan d'adressage](https://gitlab.com/Smadj33/ansible-config-switch/blob/master/plan_adressage.md)
